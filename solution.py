#!/usr/bin/env python
__author__ = "Zili Chen"

"""
Run-time of processing per query: O(mq)
"""

import os, sys
import argparse
import re
import math
import heapq

def corpus_preprocessing(line, dict_backup, corpus):
	"""Preprocess the corpus

	@para line: each line of the txt
	@para dict_backup: stores the origin documents with index (2 fields), eg: { 0 : title + author }
	@para corpus: stores the preprocessed data, eg: {'word': { doc No. contains the word : tf }}
	@rtype None
	"""
	match = re.split(r'(^\d+\s+)', line.strip())
	if match:
		sen_no = int(match[1].strip())
		value_backup = match[2].strip()
		# split title and author by \t
		field = re.split(r'\t', match[2].strip())
		dict_backup[sen_no] = [field[0], field[1]]

		# process the whole document (title + author)
		# 1. remove possessive, dog's -> dog, dogs' -> dog
		# 2. remove misc, Bront{uml}e -> Bronte  Bos|ton -> Boston
		# 3. remove single quote 'Christ is all' -> Christ is all ,except for 'm 're 'd 'll 've 't 
		# 4. split the words by space,:;?!"()[]&-_~\/  or ... 
		remove_possessive = re.sub(r'\'s\ ', ' ',value_backup.lower())
		remove_misc = re.sub(r'(s\'|\||\{\w+\})', '', remove_possessive)
		remove_quote = re.sub(r'\'(?!(m |re |d |ll |ve |t ))', '', remove_misc)
		words = re.split(r'[\s+,:;?!"()\[\]&-_~\\/]|\.{2,}', remove_quote)
		wordlist = {}
		for word in words:
			if word == '':
				continue
			# compute tf for each word, update corpus[word]
			wordlist[word] = wordlist.get(word, 0) + 1
			if corpus.has_key(word):
				corpus[word].update({sen_no : wordlist[word]})
			else :
				corpus[word] = {sen_no:wordlist[word]}

def query_preprocessing(query):
	"""Preprocess the query

	@para query: string of query
	@rtype query_words: list of words in the query
	"""
	# do part of the same process as corpus, remove possessive/-/_/&  (those may appear in a typical query)
	remove_possessive = re.sub(r'\'s\ |s\' |-|_|&', ' ',query.lower())
	query_words = re.split(r'\s+', remove_possessive.strip())

	return query_words

def precompute(corpus, n_docs):
	"""Precompute tf-idf for each word in different documents

	@para corpus: corpus after preprocessing, corpus[word] = { doc No. contains the word : tf }
	@para n_docs: total number of documents 
	@rtype None
	"""
	for outkey in corpus.keys():
		# 1. for each word, compute how many documents contain that word
		contains_sens = len(corpus[outkey])
		for innerkey in corpus[outkey].keys():
			# 2. for each documents, compute tf * idf
			tf_idf = float(corpus[outkey][innerkey]) * float(math.log(n_docs) - math.log(contains_sens))
			corpus[outkey][innerkey] = tf_idf

def cal_tf_idf(corpus, query_words, n_docs):
	"""Calculate the overall tf*idf for the query

	@para corpus: corpus after precomputing, corpus[word] = { doc No. contains the word : tf*idf }
	@para query_words: a list of query words
	@para n_docs: total number of documents 
	@rtype tf_idf_overall: the dict of overall tf*idf value, {index : score}
	"""
	tf_idf_overall = {}
	keys = corpus.keys()
	for word in query_words:
		# check whether the word is in the corpus
		# if match -> for each document, accumulate the tf-idf value for that word
		if word in keys:
			for idx in corpus[word].keys():
				tf_idf_overall[idx] = tf_idf_overall.get(idx, 0) + corpus[word][idx]
		# ignore the 's' '.' 'ed' 'ing' in the end of a word, only keep the stem (simple one)
		# but add a little penalty		
		if word.endswith('s') or (len(word) > 2 and word.endswith('.')):
			if word[:-1] in keys:   # likes -> like  Rev. -> Rev		
				for idx in corpus[word[:-1]].keys():
					tf_idf_overall[idx] = tf_idf_overall.get(idx, 0) + corpus[word[:-1]][idx] * 0.5
		elif word.endswith('ed'):
			if word[:-2] in keys:   # connected -> connect
				for idx in corpus[word[:-2]].keys():
					tf_idf_overall[idx] = tf_idf_overall.get(idx, 0) + corpus[word[:-2]][idx] * 0.5
			elif word[:-1] in keys: # used -> use
				for idx in corpus[word[:-1]].keys():
					tf_idf_overall[idx] = tf_idf_overall.get(idx, 0) + corpus[word[:-1]][idx] * 0.5
		elif word.endswith('ing'):  
			if word[:-3] in keys: # visiting -> visit
				for idx in corpus[word[:-3]].keys():
					tf_idf_overall[idx] = tf_idf_overall.get(idx, 0) + corpus[word[:-3]][idx] * 0.5

	return tf_idf_overall

def TopK(k, mydict, dict_backup):
	"""Return top k results

	@para k: integer
	@para mydict: the dict of overall tf*idf value, {index : score}
	@para dict_backup: origin documents with index (2 fields), eg: { 0 : title + author }
	@rtype None
	"""
	# heapify the dict, switch the index and score
	heap = [(value, int(key)) for key,value in mydict.items()]
	# return top10 scores
	TopK = heapq.nlargest(k, heap)
	# for the same socre, sort by index ascend
	TopK = sorted(TopK, key = lambda x: (-x[0], x[1]))
	# TopK[i][0] is tf-idf, TopK[i][1] is the No. of document
	if (len(TopK) > 0):
		for idx in range(len(TopK)):
			print '{0} {1} title: {2}  author: {3}'.format(TopK[idx][1], TopK[idx][0], dict_backup[TopK[idx][1]][0], dict_backup[TopK[idx][1]][1])
	else:
		sys.stderr.write("No match result found.\n")

if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument('txtDir', type=str, help='directory of title_author.tab.txt')
	args = parser.parse_args()

	# file path check
	if (os.path.exists(args.txtDir) == False):
		sys.stderr.write("File not found.")
		sys.exit()

	sys.stderr.write("Preprocessing corpus start.\n")

	# read file and preprocessing
	txtFile = open(args.txtDir, 'r')
	dict_backup = {}
	corpus = {}
	for line in txtFile.readlines():
		if (line.strip() == ''):
			continue
		corpus_preprocessing(line, dict_backup, corpus)
	# get the number of documents
	n_docs = dict_backup.keys()[-1] + 1
	# precompute tf-idf for each word in different documents
	precompute(corpus, n_docs)
	sys.stderr.write("Preprocessing corpus done.\n")

	while 1:
		raw_query = raw_input("\nEnter the query, or Enter ^d to quit.\n")
		if (raw_query == ''):
			sys.stderr.write("Empty query\n")
			continue
		elif (raw_query == '^d'):
			sys.stderr.write("EOF detected, exiting\n")
			sys.exit()
		# preprocess the query
		query_words = query_preprocessing(raw_query)

		# calculate overall tf*idf for the documents that contain the words in query
		tf_idf_overall = cal_tf_idf(corpus, query_words, n_docs)

		# return top 10 results
		TopK(10, tf_idf_overall, dict_backup)
